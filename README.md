# Teoria Computacional (Practicas)

### Lenguajes
- C++11
- Python3



#### Tabla de practicas 
| No. Practica | Titulo | Estado |
| ------ | ------ | ----- |
|1|Combinaciones binarias de tamaño n, de tamaño 0 a n, y graficar el numero de 1's por cadena|Sin Terminar|
|2|Generador de números primos|Terminada|
|3|Paridad binaria|Terminada|
|4|Protocolo| Terminada|
|5|Terminación '01'|Terminada|
|6|Identificador de palabras|Sin terminar|
|7|Generador de cadenas a partir de una expresión regular|Sin empezar|
|8|Automata de Pila, Numero par de 1's y 0's|Sin empezar|
|9|Generador de palíndromos|Sin empezar|
|10|Árbol de derivaciones, balanceo de parentesis|Sin empezar|
|11|Maquina de Turing|Sin empezar|

### Dependencias
```sh
python3-graphbiz
```